# Présentation GitLab CI / CD

Le 2021-03-02, pour efrei Linux sur Dicord

- Marc Gavanier
- Ingénieur efrei promotion 2014
- Lead developper Addactis Software - développement web full-stack
  - C# / dotnet core pour le back
  - TypeScript / Angular pour le front
  - Terraform / AWS pour l'infrastructure

## Introduction globale à GitLab

### Git en bref

Git est un logiciel disponible par défaut en ligne de commande dont l'objectif est de gérer des contenus rédigés sous forme de texte plat :
- Pas du tout adapté pour des fichiers binaires, compressés, Word, Excel, etc.
- Très adapté pour gerer les sources d'un projet informatique, ou ce document que vous êtes en train de lire.

git addresse (entre autre) les cas suivants :
- Plusieurs versions en parallèle d'un ensemble de documents grâce à un système de branches.
- Contrôle de la fusion de plusieurs modifications ajoutés en parallèle par plusieurs personnes.
- Historisation des modifications enregistrés sous forme d'instantanés (commits) avec la possibilité de naviguer à travers toutes ces versions.
- Travail asyncrone en local sans dépendre d'une connexion permanante à une source avec laquelle se syncroniser à la moindre modification.

Ma recommendation : utiliser git en ligne de commande si vous débutez, puis passez à un client graphique une fois que vous maîtrisez l'outil si vous en éprouvez le besoin.

Message à ceux qui ne l'utilise pas encore : vous devriez **vraiment** l'envisager.

Un bon outil pour commencer à apprendre git : [Learn Git Branching](https://learngitbranching.js.org/)

### GitHub vs GitLab

#### Qu'est-ce que GitHub ou GitLab

Ce sont deux services web assez similaires qui proposent d'hébreger des dépôts git pour en offrir l'accès à plusieurs membres d'une et ainsi faciliter la mise en commun du travail de chacun.

Ils proposent donc tous les deux un modèle centralisé avec lequel chaque contributeur doit publier ses propres changements faits sur son dépôt local ou récupérer les changements d'autres personnes qui auraient été publiés.

À cette fonctionalité de base qui constitue le coeur de leur logique ils ajoutent chacun une série d'outil pour faciliter la collaboration, comme la possibilité de gérer des droits d'accès, des outils pour relire et commenter le code, etc.

#### Quelques différences notables

- GitLab est open source alors que GitHub ne l'est pas
- GitHub est beaucoup plus populaire et offre plus de visibilité aux projets qui y sont publiés
- Gitlab permet de gérer une hiérarchie de projets
- Les pull requests de GitHub sont appelées merge requests sur GitLab et permettent de [relire et commenter le code](https://gitlab.com/cambonie/tutoef/-/merge_requests/1/diffs)
- GitLab gère des pipelines de déploiement et d'intégration continue alors que GitHub doit recourir à des outils/services tiers tels que [CircleCI](https://circleci.com/), [Travis CI](https://travis-ci.org/) ou [Jenkins](https://www.jenkins.io/)
- Et plein d'autre choses [Package et registres](https://docs.gitlab.com/ee/user/packages/), [Getsion de projet](https://docs.gitlab.com/ee/user/project/issues/), [Hébergement de site statiques](https://docs.gitlab.com/ee/user/project/pages/), etc.

## Explication des concepts d'intégration continue et de déploiement continue

### Intégration continue

En anglais continuous integration (CI) représente la simplification et l'automatisation de la mise en commun du travail de chaque contributeur dans le but de pouvoir le faire sans accrocs et donc (très) fréquement.
En soit git permet déjà de faire cela, mais le concept de CI ajoute la maîtrise du code publié en s'assurant par exemple que le code fonctionne, que les conventions établies sont respectées, que les fonctionalités produites ne cassent pas autre chose ailleurs, en bref, cela assure que la personne qui va récupérer ce code ne va pas regretter de l'avoir fait.

### Déploiement continue

En anglais continuous deployment + continuous delivery (CD) va un cran plus loin que l'intégration continue en publiant le projet dans son contexte de production :
- En ligne pour un site internet
- Publié sur un store pour une application mobile
- Accessible depuis un gestionnaire de paquets pour une bibliothèque
- Disponible sur Steam pour un jeu vidéo
- etc.
Si par abus CD peut désigner déploiement ou livraison continue, on distingue quand même deux cas :
- La livraison continue assure que le projet est dans un état qui est prêt à être déployé, mais cette étape reste manuelle
- Le déploiement continu automatise la mise en production.

### Quel est l'intérêt de tout ça ?

- Maîtiser de la qualité du code produit et éviter les déconvenues liés à un code mis à disposition alors qu'il ne devrait pas
- Éliminter des taches qui peuvent être (très) laborieuses, par exemple la publication d'une application cross platform sur le Play Store et sur l'Apple store
- Éviter de faire des erreurs de manipulation comme par exemple se tromper de serveur lorsque on synchronise les sources d'un site web en ftp

En bref ajouter de la fluidité dans le développement et de la sérénité dans le déploiement

## Présentation de GitLab CI/CD

### Concepts de pipeline, stage et job

- Pipeline : le flux d'étapes à travers lequel doit passer le code avant de pouvoir être considéré comme prêt ou déployé
- Stage : étape qui constitue le pipeline, par défaut les stages s'executent séquentiellement
- Job : une tâche à effectuer dans un stage, le stage est considéré complet une fois que tous ses jobs sont terminés

### L'intérêt de Docker dans ce cas

Le problème c'est que dans les jobs, nous avons besoin de lancer des applications pour effectuer les actions correspondantes. Pour faire cela GitLab ne nous met pas à disposition un serveur sur lequel nous porions installer tout ce dont nous avons besoin, mais encourage plutôt à utiliser des images Docker.

Docker repose sur un système de couche :
- La couche la plus basse se repose sur le noyeau du système hôte, l'OS n'est donc pas embarqué.
- chaque couche vient se superposer en ajoutant des applications ou des fichiers.

Une image Docker présente donc le double avantage d'être plutôt légère et rapide car le noyeau n'est pas embarqué et n'a pas besoin d'être démarré et offre en plus de cela la possibilité d'être spécialisé pour un besoin donné.

#### Docker Hub

Et ce qui est encore mieux, c'est qu'on a probablement pas besoin de créer ses propres images, parcequ'il y en existe des publiques pour à peu près  n'importe quel usage sur [Docker Hub](https://hub.docker.com/search?q=&type=image/)

[Alpine Linux](https://hub.docker.com/_/alpine?tab=tags&page=1&ordering=last_updated&name=3.13.2) est souvent utilisé comme image de base, car elle tiens dans 2.69 MB

Petit point sécurité : attention aux images sur Docker Hub, il faut bien véfifier ce qu'il y a dessus parce que rien ne garenti qu'il n'y a rien qui puisse vous piquer vos identifiants ou vos clés qui pourraient par exemple transiter pour faire des déploiements :
- Ne faites conficance qu'au images officielles et aux éditeurs vérifiés (et encore, il y a même des optimisations à faire parfois)
- Si vous avez un doute, faite votre propre image (faites là, de toute façon ça fait progresser :D)
- Pour info il existe des outil d'analyse de la sécurité des images

## Démonstration

### Présentation rapide du projet Angular à outiller

#### Création de l'application

Aller dans le dossier du projet :

```bash
cd workspace/teach/gitlab
```

Vous avez besoin de [node](https://nodejs.org/en/) pour executer les commandes suivantes.

Installer Angular CLI et un schematic permettant d'utiliser eslint au lieu de tslint qui n'est plus maintenu :
```bash
npm i -g @angular/cli @angular-devkit/{core,schematics} @angular-eslint/schematics
```

Générer un projet Angular avec eslint :

```bash
ng new --skip-install --skip-git
ng add @angular-eslint/schematics
```
- Nommer le `my-app`
- Ne pas ajouter les routes d'Angular (N)
- Sélectionner SCSS en tant que preprocessor de style

Aller à la racine du projet :

```bash
cd my-app
```

Installer les [node modules](https://cdn.evilmartians.com/front/posts/autoprefixer-7-browserslist-2-released/blackhole-9623739.png) :

```bash
yarn
```

Installer le scematic pour utiliser Jest au lieu de Jasmine et Karma pour les tests et style lint qui permet de vérifier la syntaxe des fichier de style :

```bash
yarn add @briebug/jest-schematic stylelint stylelint-config-standard stylelint-scss -D
```

Lancer la commande suivante pour utiliser Jest au lieu de Jasmine et Karma :

```bash
ng g @briebug/jest-schematic:add
```

Ajouter le fichier de configuration de stylelint :

```
touch .stylelintrc.json
```

Ouvrir le fichier de configuration de stylelint avec un IDE, puis ajouter des règles :

```json
{
  "plugins": [
    "stylelint-scss"
  ],
  "rules": {
    "at-rule-no-unknown": null,
    "scss/at-rule-no-unknown": true,
    "scss/selector-no-redundant-nesting-selector": true
  }
}
```

Add stylelint script in `package.json` :

```json
"stylelint": "stylelint \"**/*.scss\"",
```

#### Création du dépôt sur GitLab

- Aller dans le [groupe qui va contenir le projet](https://gitlab.com/kyappy.teach/presentations)
- Cliquer sur `New project` et sélectionner `Create blank project`
- Mettre le nom `my-app`
- Choisir `Public` comme niveau de visibilité
- Suivre les instructions de `Push an existing folder`

#### Node, npm et yarn

- Node : permet d'executer du JavaScript en dehors de son navigateur
- npm (Node Package Manager) : gestionaire de paquet par défaut pour node
- yarn : gestionaire de paquet pour node crée par Facebook, il offre plusieurs améliorations notemment en terme de rapidité d'installation, on peut utiliser npm pour l'installer `npm install yarn --global`

#### Build

Permet de construire un projet web statique à partir des sources :

```bash
ng build --prod
```

Le drapeau `prod` permet de faire des optimisations pour réduire la taille de l'application à déployer dans un contexte de production.

Le build est généré dans le dossier `dist`

#### Lint

Permet de vérifier si la syntaxe du code TypeScript respecte les conventions étables dans le fichier `.eslintrc.json`

Pour vérifier que la syntaxe TypeScript est correcte, lancer :

```bash
ng lint
```

Pour vérifier que la syntaxe SCSS est correcte, lancer :

```bash
yarn stylelint
```

#### Test

Pour vérifier que l'implémentation correpond au comportement attendu :

```bash
ng test
```

### Écriture des scripts d'automatisation

Créer un fichier `.gitlab-ci.yml` à la racine du projet, il va contenir la configuration du pipeline.

#### Lint

Déclarer le stage `lint`

```yml
stages:
  - lint
```

Puis ajouter le job `typescript` pour vérifier la syntaxe du code avec `lint` :

```yml
typescript:
  stage: lint
  image: node:lts-alpine3.13
  script:
    - yarn install --frozen-lockfile
    - yarn run ng lint
```

Toujour dans le stage `lint`, ajouter le job `scss` pour vérifier la syntaxe du style avec `stylelint` :

```yml
scss:
  stage: lint
  image: node:lts-alpine3.13
  script:
    - yarn install --frozen-lockfile
    - yarn run stylelint
```

À la fin de cette étape, le fichier `.gitlab-ci.yml` ressemble à cela :

```yml
stages:
  - lint

typescript:
  stage: lint
  image: node:lts-alpine3.13
  script:
    - yarn install --frozen-lockfile
    - yarn run ng lint

scss:
  stage: lint
  image: node:lts-alpine3.13
  script:
    - yarn install --frozen-lockfile
    - yarn run stylelint
```

Publier cette itération :

```bash
git add .gitlab-ci.yml && git commit -m"add lint stage containing jobs for typescript and scss files"
git push origin master
```

Aller voir le pipeline crée sur gitlab dans le menu `CI / CD` sur la gauche.

#### Ajouter un peu d'organisation

Créer un dossier `.gitlab` à la racine du projet, y ajouter un dossier `ci`, puis créer un dossier `lint` dans le dossier `ci`.

Ajouter les fichiers `typescript.yml` et `scss.yml` dans le dossier `lint` et déplacer la description des deux jobs depuis le fichier `.gitlab-ci.yml` vers les fichiers correspondants dans le dossier `lint`

Crer un fichier `_lint.yml` dans le dossier `lint` et y déclarer les références vers les fichiers `typescript.yml` et `scss.yml`

```yml
include:
  - '/.gitlab/ci/lint/scss.yml'
  - '/.gitlab/ci/lint/typescript.yml'
```

Maintenant il ne reste plus qu'à référencer le fichier `_lint.yml` dans le fichier `.gitlab-ci.yml` :

```yml
stages:
  - lint

include:
  - '/.gitlab/ci/lint/_lint.yml'
```

Publier cette itération :

```bash
git add .gitlab-ci.yml .gitlab/ && git ci -m"refactor gitlab ci file organization"
git push origin master
```

Le pipeline doit continuer de fonctionner de la même manière.

#### Build

Ajouter le dossier `.gitlab/ci/build`, puis y créer les fichiers `_build.yml`, `development.yml` et `production.yml`.

Déclarer les références vers les fichiers `development.yml` et `production.yml` dans `_build.yml` :
 
 ```yml
 include:
  - '/.gitlab/ci/build/development.yml'
  - '/.gitlab/ci/build/production.yml'
 ```
 
Ajouter le job pour le build de developement dans `development.yml` :

```yml
development:
  stage: build
  image: node:lts-alpine3.13
  script:
    - yarn install --frozen-lockfile
    - yarn run ng build --output-path=development
  artifacts:
    expire_in: 1 day
    when: on_success
    paths:
      - development
```

Faire la même chose pour le build de production dans `production.yml` :

```yml
production:
  stage: build
  image: node:lts-alpine3.13
  script:
    - yarn install --frozen-lockfile
    - yarn run ng build --prod --output-path=production
  artifacts:
    expire_in: 1 day
    when: on_success
    paths:
      - production
```

Ajouter le stage build et la référence vers `_build.yml` dans le fichier `.gitlab-ci.yml` :

```yml
stages:
  - lint
  - build

include:
  - '/.gitlab/ci/lint/_lint.yml'
  - '/.gitlab/ci/build/_build.yml'
```

Publier cette itération :

```bash
git add .gitlab-ci.yml .gitlab/ci/build/ && git ci -m"add build stage containing development and production jobs"
git push origin master
```

Le pipeline doit maintenant contenir un nouveau stage nommé build composé de deux jobs.

#### Test

Ajouter le dossier `.gitlab/ci/test`, puis y créer les fichiers `_test.yml`, `unit.yml`.

Déclarer la références vers le fichier `unit.yml` dans `_test.yml` :
 
 ```yml
 include:
  - '/.gitlab/ci/test/unit.yml'
 ```
 
Ajouter le job pour les test unitaires dans `unit.yml` :

```yml
unit:
  stage: test
  image: node:lts-alpine3.13
  script:
    - yarn install --frozen-lockfile
    - yarn run ng test --collectCoverage
```

Ajouter le stage test et la référence vers `_test.yml` dans le fichier `.gitlab-ci.yml` :

```yml
stages:
  - lint
  - build
  - test

include:
  - '/.gitlab/ci/lint/_lint.yml'
  - '/.gitlab/ci/build/_build.yml'
  - '/.gitlab/ci/test/_test.yml'
```

Publier cette itération :

```bash
git add .gitlab-ci.yml .gitlab/ci/test/ && git ci -m"add test stage containing unit job"
git push origin master
```

#### Deploy

Ajouter le dossier `.gitlab/ci/deploy`, puis y créer les fichiers `_deploy.yml`, `pages.yml`.

Déclarer la références vers le fichier `pages.yml` dans `_deploy.yml` :
 
 ```yml
 include:
  - '/.gitlab/ci/deploy/pages.yml'
 ```
 
Ajouter le job pour le déploiement sur gitlab pages dans `pages.yml` :

```yml
pages:
  stage: deploy
  image: alpine:3.13.2
  script:
    - mv /dist/production/* /public/
  artifacts:
    paths:
      - public
```

Ajouter le stage deploy et la référence vers `_deploy.yml` dans le fichier `.gitlab-ci.yml` :

```yml
stages:
  - lint
  - build
  - test
  - deploy

include:
  - '/.gitlab/ci/lint/_lint.yml'
  - '/.gitlab/ci/build/_build.yml'
  - '/.gitlab/ci/test/_test.yml'
  - '/.gitlab/ci/deploy/_deploy.yml'
```

Publier cette itération :

```bash
git add .gitlab-ci.yml .gitlab/ci/deploy/ && git ci -m"add deploy stage containing pages job"
git push origin master
```
